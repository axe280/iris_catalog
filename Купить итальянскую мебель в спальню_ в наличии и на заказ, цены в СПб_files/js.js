jQuery(function($) {

    // Каталог
    $('.aside-catalog').each(function(){
        var $as = $(this);
        var catalogUrl = $as.data('url');
        var parseArrInt = function(arr){
            var ids = [];
            for (var i in arr) ids.push(parseInt(arr[i]));
            return ids;
        };
        var settings = {
            filter_producers: $as.data('pr') ? parseArrInt(String($as.data('pr')).split('-')) : [],
            filter_styles: $as.data('s') ? parseArrInt(String($as.data('s')).split('-')) : [],
            filter_in_stock: $as.data('in') || 0
        };

        // console.log(settings);
        var reloadProducts = function() {
            // console.log(settings);
            var u = '';
            if (settings.filter_producers.length == 1) {
                u += $('.filter-producers a[data-id='+settings.filter_producers[0]+']').data('skey') + '/';
            }
            if (settings.filter_styles.length == 1) {
                u += $('.filter-styles a[data-id='+settings.filter_styles[0]+']').data('skey') + '/';
            }
            if (settings.filter_producers.length > 1) {
                u += settings.filter_producers.length ? (u.indexOf('?') === -1 ? '?' : '&') + 'pr=' + settings.filter_producers.join('-') : '';
            }
            if (settings.filter_styles.length > 1) {
                u += settings.filter_styles.length ? (u.indexOf('?') === -1 ? '?' : '&') + 's=' + settings.filter_styles.join('-') : '';
            }
            var url = catalogUrl + u;
            $.getJSON(url, function(d){
                if (d && d.type === 'ok') {
                    if (!parseInt(d.data.count) && settings.filter_producers.length) {
                        settings.filter_producers = [];
                        $('.filter-producers .catalog-nav--checked').removeClass('catalog-nav--checked');
                        return reloadProducts();
                    }
                    $('#products-list').html(d.data.productsHtml);
                    if (d.data.h1) {
                        $('.catalog-head h1').html(d.data.h1);
                    }
                    $('#products-list').html();
                    if (d.data.producersCounts) {
                        $('.filter-producers a').each(function(i, el){
                            var data_id = el.getAttribute('data-id');
                            var response_data = d.data.producersCounts.filter(function(res){
                                return res.id == data_id;
                            });
                            if(!response_data.length || !parseInt(response_data[0].count))
                                $(el).parent().slideUp();
                            else
                                $(el).parent().slideDown().find('span').html(response_data[0].count).show();
                        });
                        /*$.each(d.data.producersCounts, function(i,a){
                            if (parseInt(a.count)) {
                                $('.filter-producers a[data-id='+a.id+']').parent().slideDown().find('span').html(a.count).show();
                            } else {
                                $('.filter-producers a[data-id='+a.id+']').parent().slideUp();
                            }
                        });*/
                    }
                    if (d.data.stylesCounts) {
                        $('.filter-styles a').each(function(i, el){
                            var data_id = el.getAttribute('data-id');
                            var response_data = d.data.stylesCounts.filter(function(res){
                                return res.id == data_id;
                            });
                            if(!response_data.length || !parseInt(response_data[0].count))
                                $(el).parent().slideUp();
                            else
                                $(el).parent().slideDown().find('span').html(response_data[0].count).show();
                        });
                        /*$.each(d.data.stylesCounts, function(i,a){
                            if (parseInt(a.count)) {
                                $('.filter-styles a[data-id='+a.id+']').parent().slideDown().find('span').html(a.count).show();
                            } else {
                                $('.filter-styles a[data-id='+a.id+']').parent().slideUp();
                            }
                        });*/
                    }
                    $('.show-animation').addClass("hidden").viewportChecker({
                        classToAdd: 'visible animated',
                        offset: 100
                    });

                    var startWindowScroll = 0;
                    $('#products-list .open-modal').magnificPopup({
                        fixedContentPos: true,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        type: 'inline',
                        midClick: true,
                        callbacks: {
                            open: function(){
                                if ( $('.mfp-content').height() < $(window).height() ){
                                    $('body').on('touchmove', function (e) {
                                        e.preventDefault();
                                    });
                                }
                            },
                            close: function() {
                                $(window).scrollTop(startWindowScroll);
                                $('body').off('touchmove');
                            }
                        }
                    });

                    $('#products-list .open-modal').on('mfpOpen', function(e) {
                        $('.form-order').show();
                        $('.thanks-message').hide();
                        var type = $(this).data('type');
                        var title = $(this).data('title');
                        var title2 = $(this).data('title2');
                        var onlyPhone = $(this).data('onlyphone');
                        var withText = $(this).data('withtext');
                        var msg = $(this).data('msg');
                        var textplaceholder = $(this).data('textplaceholder');
                        var m = $.magnificPopup.instance;
                        $(m.content).find('.form-head .site-h2-tt').html(title);
                        $(m.content).find('.form-head .site-h4-tt').html(title2);
                        $(m.content).find('input[name=type]').val(type);
                        $(m.content).find('input[name=msg]').val(msg);
                        if (title2) {
                            $('.form-head').css('margin-bottom', '30px');
                        }
                        if (onlyPhone) {
                            $('label[for=formPhone]').html('Телефон');
                        }
                        if (withText) {
                            $('#formTextBlock').show();
                            if (textplaceholder) {
                                $('label[for=formMsg]').html(textplaceholder);
                            }
                        } else {
                            $('#formTextBlock').hide();
                        }
                    });
                }
            });
            if (history.pushState) {
                history.pushState(null, null, url);
            }

            // $('.filter-c-btn').removeClass('_active');
            // if (settings.filter_categories.length || settings.filter_colors.length || settings.filter_brands.length || settings.filter_prices.length) {
            //     $('.filter-c-btn').addClass('_active').find('.filter-c-btn__text').html('Отфильтровано');
            // } else {
            //     $('.filter-c-btn .filter-c-btn__text').html('Фильтровать');
            // }
        };

        $(document).on('click', '.filter-categories a', function(e){
            e.preventDefault();
            var $a = $(this);
            var id = $a.data('id');
            var d = $a.closest('.aside-catalog-nav__inner-list');
            if (!$a.closest('.aside-catalog-nav-list').length) {
                if (!d.hasClass('cat-nav--open')) {
                    d.addClass('cat-nav--open');
                } else {
                    d.removeClass('cat-nav--open');
                }
            }
            // if (d.find('.aside-catalog-nav-list').length && !$a.closest('.aside-catalog-nav-list').length) {
            //     return;
            // }
            // if ($a.is('.catalog-nav--checked')) {
            //     $('.c-filter-open .catalog-filter-btn').click();
            //     return;
            // }

            if (!$a.parent().find('.aside-catalog-nav-list').length) {
                $('.c-filter-open .catalog-filter-btn').click();
                // return;
            }

            $('.filter-categories a.catalog-nav--checked').removeClass('catalog-nav--checked');
            $a.addClass('catalog-nav--checked');
            var d = $a.closest('.aside-catalog-nav__inner-list');
            // if (!d.hasClass('cat-nav--open')) {
            //     // $('.filter-categories .cat-nav--open').removeClass('cat-nav--open');
            //     d.addClass('cat-nav--open');
            // } else {
            //     d.removeClass('cat-nav--open');
            // }
            catalogUrl = $a.attr('href');
            if ($('.c-filter-open .catalog-filter-btn').length) {
                settings.filter_producers = [];
                settings.filter_styles = [];
                $('.filter-styles .catalog-nav--checked').removeClass('catalog-nav--checked');
                $('.filter-producers .catalog-nav--checked').removeClass('catalog-nav--checked');
            }
            reloadProducts();
        });

        $(document).on('click', '.filter-producers li a', function(e){
            e.preventDefault();
            var $a = $(this);
            if ($a.is('.catalog-nav--checked')) {
                $a.removeClass('catalog-nav--checked');
            } else {
                $a.addClass('catalog-nav--checked');
            }
            // $a.parent().siblings().each(function(i,a){
            //     $(a).find('a').removeClass('catalog-nav--checked')
            // });
            settings.filter_producers = [];
            $('.filter-producers .catalog-nav--checked').each(function(i,a){
                if ($(a).data('id')) {
                    settings.filter_producers.push($(a).data('id'));
                }
            });
            reloadProducts();
        });

        $(document).on('click', '.filter-styles li a', function(e){
            e.preventDefault();
            var $a = $(this);
            if ($a.is('.catalog-nav--checked')) {
                $a.removeClass('catalog-nav--checked');
            } else {
                $a.addClass('catalog-nav--checked');
            }
            settings.filter_styles = [];
            $('.filter-styles .catalog-nav--checked').each(function(i,a){
                if ($(a).data('id')) {
                    settings.filter_styles.push($(a).data('id'));
                }
            });
            reloadProducts();
        });

        $(document).on('click', '.pagination a', function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            $.getJSON(url, function(d){
                if (d && d.type === 'ok') {
                    $('#products-list').html(d.data.productsHtml);
                    if (d.data.h1) {
                        $('.catalog-head h1').html(d.data.h1);
                    }
                    $("html, body").animate({ scrollTop: $('#products-list').offset().top - 40}, 600);
                    $('.show-animation').addClass("hidden").viewportChecker({
                        classToAdd: 'visible animated',
                        offset: 100
                    });

                    var startWindowScroll = 0;
                    $('#products-list .open-modal').magnificPopup({
                        fixedContentPos: true,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        type: 'inline',
                        midClick: true,
                        callbacks: {
                            open: function(){
                                if ( $('.mfp-content').height() < $(window).height() ){
                                    $('body').on('touchmove', function (e) {
                                        e.preventDefault();
                                    });
                                }
                            },
                            close: function() {
                                $(window).scrollTop(startWindowScroll);
                                $('body').off('touchmove');
                            }
                        }
                    });

                    $('#products-list .open-modal').on('mfpOpen', function(e) {
                        $('.form-order').show();
                        $('.thanks-message').hide();
                        var type = $(this).data('type');
                        var title = $(this).data('title');
                        var title2 = $(this).data('title2');
                        var onlyPhone = $(this).data('onlyphone');
                        var withText = $(this).data('withtext');
                        var msg = $(this).data('msg');
                        var textplaceholder = $(this).data('textplaceholder');
                        var m = $.magnificPopup.instance;
                        $(m.content).find('.form-head .site-h2-tt').html(title);
                        $(m.content).find('.form-head .site-h4-tt').html(title2);
                        $(m.content).find('input[name=type]').val(type);
                        $(m.content).find('input[name=msg]').val(msg);
                        if (title2) {
                            $('.form-head').css('margin-bottom', '30px');
                        }
                        if (onlyPhone) {
                            $('label[for=formPhone]').html('Телефон');
                        }
                        if (withText) {
                            $('#formTextBlock').show();
                            if (textplaceholder) {
                                $('label[for=formMsg]').html(textplaceholder);
                            }
                        } else {
                            $('#formTextBlock').hide();
                        }
                    });
                }
            });
            if (history.pushState) {
                history.pushState(null, null, url);
            }
        });

        $(document).on('click', 'button[type=reset], .clear-filter', function(e){
            e.preventDefault();
            settings.filter_producers = [];
            settings.filter_styles = [];
            $('.filter-styles .catalog-nav--checked').removeClass('catalog-nav--checked');
            $('.filter-producers .catalog-nav--checked').removeClass('catalog-nav--checked');
            // reloadProducts();
            $('.clear-filter-click').click();
            $("html, body").animate({ scrollTop: $('#products-list').offset().top - 40}, 600);
            if ($('.c-filter-open').length) {
                $('.catalog-filter-btn').click();
            }
        });

    });

    // Печать страницы
    $(document).on('click', '.icon-aside--print', function(e){
        e.preventDefault();
        window.print();
    });

});