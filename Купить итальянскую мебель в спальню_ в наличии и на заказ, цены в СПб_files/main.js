jQuery(function($){

    document.addEventListener('gesturestart', function (e) {
        e.preventDefault();
    });

    if ('ontouchstart' in window) {
    /*$(window).load(function(){
            $('.block-car-img, .full-screen-img').each(function() {
                new RTP.PinchZoom($(this));
            });
    });*/
    }

  //form elements
  $('input[placeholder], textarea[placeholder]').placeholder();
  $(".mask-phone").mask("+7 (999) 999-99-99");

  //checkbox style (radio/select/file)
  jcf.replaceAll();

  // form
    $(document).on('change', '#pCh1', function(e){
        if ($(this).is(':checked')) {
            $(this).closest('.form-bottom-wrapp').find('button').removeAttr('disabled');
        } else {
            $(this).closest('.form-bottom-wrapp').find('button').attr('disabled', 'disabled');
        }
    });

    $('.form-order').submit(function(e){
        e.stopPropagation();
        e.preventDefault();

        var form = $(this);
        var name = form.find('input[name=name]').val();
        var phone = form.find('input[name=phone]').val();
        var type = form.find('input[name=type]').val();
        var msg = form.find('input[name=msg]').val();

        // form.find('input[name=name]').parent().addClass('error-input').find('label').html('Ваше имя');
        // form.find('input[name=phone]').parent().addClass('error-input').find('label').html('Телефон');
        //
        // if (!name.trim().length) {
        //     form.find('input[name=name]').parent().addClass('error-input').find('label').html('Введите Ваше имя');
        // }
        // if (!phone.trim().length) {
        //     form.find('input[name=name]').parent().addClass('error-input').find('label').html('Заполните телефон');
        // }

        form.find('button:submit').attr('disabled', 'disabled').html('Подождите...');
        $.ajax({
            method: "POST",
            url: "/send/",
            dataType: "json",
            data: { name:name, phone:phone, type:type, msg:msg }
        })
        .done(function(data) {
            form.find('button:submit').removeAttr('disabled').html('Отправить');
            if (data.type === 'ok') {
                $('.form-order').slideUp();
                $('.thanks-message').slideDown();
                form.find('input[name=name]').val('');
                form.find('input[name=phone]').val('');
            } else {
                $(form).find('.alert-error').html(data.data).show();
            }
        });

        yaCounter48436283.reachGoal('sendform');
    });

    var startWindowScroll = 0;
    $('.open-modal').each(function(){
        $(this).magnificPopup({
            fixedContentPos: true,
            fixedBgPos: true,
            overflowY: 'auto',
            type: 'inline',
            midClick: true,
            callbacks: {
                open: function(){
                    if ( $('.mfp-content').height() < $(window).height() ){
                        $('body').on('touchmove', function (e) {
                            e.preventDefault();
                        });
                    }
                },
                close: function() {
                    $(window).scrollTop(startWindowScroll);
                    $('body').off('touchmove');
                }
            }
        });
        var modalOpen = function(e) {
            $('.form-order').show();
            $('.thanks-message').hide();
            var type = $(this).data('type');
            var title = $(this).data('title');
            var title2 = $(this).data('title2');
            var onlyPhone = $(this).data('onlyphone');
            var withText = $(this).data('withtext');
            var msg = $(this).data('msg');
            var textplaceholder = $(this).data('textplaceholder');
            var m = $.magnificPopup.instance;
            $(m.content).find('.form-head .site-h2-tt').html(title);
            $(m.content).find('.form-head .site-h4-tt').html(title2);
            $(m.content).find('input[name=type]').val(type);
            $(m.content).find('input[name=msg]').val(msg);
            if (title2) {
                $('.form-head').css('margin-bottom', '30px');
            }
            if (onlyPhone) {
                $('label[for=formPhone]').html('Телефон');
            }
            if (withText) {
                $('#formTextBlock').show();
                if (textplaceholder) {
                    $('label[for=formMsg]').html(textplaceholder);
                }
            } else {
                $('#formTextBlock').hide();
            }
        };
        $(this).on('mfpOpen', modalOpen);
    });

    $('.open-modal2').magnificPopup({
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: 'auto',
        type: 'inline',
        midClick: true,
        callbacks: {
            open: function(){
                if ( $('.mfp-content').height() < $(window).height() ){
                    $('body').on('touchmove', function (e) {
                        e.preventDefault();
                    });
                }
            },
            close: function() {
                $(window).scrollTop(startWindowScroll);
                $('body').off('touchmove');
            }
        }
    });
    $('.open-modal3').magnificPopup({
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: 'auto',
        type: 'inline',
        midClick: true,
        callbacks: {
            open: function(){
                if ( $('.mfp-content').height() < $(window).height() ){
                    $('body').on('touchmove', function (e) {
                        e.preventDefault();
                    });
                }
            },
            close: function() {
                $(window).scrollTop(startWindowScroll);
                $('body').off('touchmove');
            }
        }
    });

  //   $('.open-modal').magnificPopup({
  //   type:'inline',
  //   midClick: true
  // });

    var modalOpen = function(e) {
        $('.form-order').show();
        $('.thanks-message').hide();
        var type = $(this).data('type');
        var title = $(this).data('title');
        var title2 = $(this).data('title2');
        var onlyPhone = $(this).data('onlyphone');
        var withText = $(this).data('withtext');
        var msg = $(this).data('msg');
        var m = $.magnificPopup.instance;
        $(m.content).find('.form-head .site-h2-tt').html(title);
        $(m.content).find('.form-head .site-h4-tt').html(title2);
        $(m.content).find('input[name=type]').val(type);
        $(m.content).find('input[name=msg]').val(msg);
        if (title2) {
            $('.form-head').css('margin-bottom', '30px');
        }
        if (onlyPhone) {
            $('label[for=formPhone]').html('Телефон');
        }
        if (withText) {
            $('#formTextBlock').show();
        } else {
            $('#formTextBlock').hide();
        }
    };

    $('.open-modal').on('mfpOpen', modalOpen);
    $('.open-modal2').on('mfpOpen', modalOpen);
    $('.open-modal3').on('mfpOpen', modalOpen);

  $('.close-modal').click(function() {
    $.magnificPopup.close();
  });

  if (false && !('ontouchstart' in window)) {
      $(".gallery-open").lightGallery({
          mousewheel: false,
          speed: 1500
      });
  } else {
    $(".gallery-open .prod-inline-item").click(function(){
      var index = $(this).index();
      var n = $(this).attr('data-n');
      $('.folio-modal[data-n="'+ n +'"] .product-carousel').trigger('to.owl.carousel', [0, 0]);

      $('.folio-modal[data-n="'+ n +'"]').addClass('_open');
      setTimeout(function(){
        $('body').addClass('lg-on');
      }, 500);
    });
    
    $('.gallery-open a').click(function(e){
      e.preventDefault();
    });

    $(".folio-modal .lg-close").click(function(){
      $('body').removeClass('lg-on');
      $(this).closest('.folio-modal').removeClass('_open');
    });
    
    $(".folio-modal").scroll(function(e){
      e.preventDefault();
    });
  }

  if ($('.card-carousel-b').length) {
      // модальное окно для галереи продукта
      $(document).on('click', '.gallery-open img', function(){
          var n = $(this).attr('data-n');
          $('.folio-modal .product-carousel').trigger('to.owl.carousel', [n, 0]);
          $('.folio-modal').addClass('_open');
          setTimeout(function(){
              $('body').addClass('lg-on');
          }, 500);
      });
  }

    //popup gallery img
  // $('.gallery-open').each(function() {
  //   $(this).magnificPopup({
  //     delegate: '.gallery-item',
  //     type: 'image',
  //     tLoading: 'Грузим картинку #%curr%...',
  //     removalDelay: 300, //delay removal by X to allow out-animation
  //     callbacks: {
  //       beforeOpen: function() {
  //          this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure animated ' + this.st.el.attr('data-effect'));
  //       }
  //     },
  //     fixedContentPos: true,
  //     gallery: {
  //       enabled: true,
  //       navigateByImgClick: true,
  //       tCounter: '',
  //       preload: [0,1] // Will preload 0 - before current, and 1 after the current image
  //     },
  //     image: {
  //       tError: '<a href="%url%">Картинка #%curr%</a> не найдена.',
  //       titleSrc: function(item) {
  //         return item.el.attr('title');
  //       }
  //     }
  //   });
  // });


  // Scrool menu fixed
  $(window).scroll(function() {
    if ($(this).scrollTop() > $('#main').offset().top){  
      $('body').addClass("sticky-catalog");
    }
    else{
      $('body').removeClass("sticky-catalog");
    }
  });

  // add-open-class
  $('.menu-icon-wrap').click(function(){
   if($(this).parent().is('.menu-mobile-open')){
     $(this).parent().removeClass('menu-mobile-open');
     $('body').removeClass('menu-open-wrapper-page');
   }else{
     $(this).parent().addClass('menu-mobile-open');
     $('body').addClass('menu-open-wrapper-page');
     $('.aside-catalog').removeClass('aside-c-open');
     $('.catalog-filter-btn').parent().removeClass('c-filter-open');
   }
  });

  // add-open-class
  $('.catalog-filter-btn').click(function(){
   if($(this).parent().is('.c-filter-open')){
    $(this).parent().removeClass('c-filter-open');
    $('.aside-catalog').removeClass('aside-c-open');
    $('body').removeClass('menu-open-wrapper-page');
   }else{
    $(this).parent().addClass('c-filter-open');
    $('.aside-catalog').addClass('aside-c-open');
    $('body').addClass('menu-open-wrapper-page');
   }
  });

  $('.mobile-filter-close').click(function(){
    $('.catalog-head__aside').removeClass('c-filter-open');
    $('body').removeClass('menu-open-wrapper-page');
    $('.aside-catalog').removeClass('aside-c-open');
  });

  // add-open-class
  $('.more-txt-btn .bt').click(function(){
   if($('.st-text-wrapp').is('.st-text-open')){
    $('.st-text-wrapp').removeClass('st-text-open');
   }else{
    $('.st-text-wrapp').addClass('st-text-open');
    $('.more-txt-btn .bt').remove();
   }
  });

  // add-open-class
  $('.menu-dd-open-link').click(function(){
   if($(this).parent().is('.menu-dd-open')){
    $(this).parent().removeClass('menu-dd-open');
   }else{
    $(this).parent().addClass('menu-dd-open');
   }
  });

  // add-open-class
  $('.m-slide-fix-point').click(function(){
   if($(this).is('.m-slide-fix-point-open')){
    $(this).removeClass('m-slide-fix-point-open');
   }else{
    $(".m-slide-fix-point").removeClass("m-slide-fix-point-open");
    $(this).addClass('m-slide-fix-point-open');
   }
  });


  $(document).bind('click touchstart', function(e) {
    var $clicked = $(e.target);
    if (! $clicked.hasClass("m-slide-fix-point")){
      $(".m-slide-fix-point").removeClass("m-slide-fix-point-open");
    }
  });


  //carousel

  $('.card-full-screen-carousel').owlCarousel({
    loop: false,
    nav: true,
    dots: false,
    items: 1,
    autoplay: true,
    autoplayTimeout: 10000,
    mouseDrag: false,
    touchDrag: true,
    navSpeed: 1000,
    smartSpeed: 1000,
    responsive : {
      // breakpoint from 768 up
      768 : {
          touchDrag: false
      }
    }
  });


  $('.catalog-carousel').owlCarousel({
    loop: true,
    nav: false,
    dots: true,
    items: 1,
    autoplay: false,
    autoplayTimeout: 5000,
    mouseDrag: true,
    touchDrag: true,
    navSpeed: 1000,
    smartSpeed: 1000,
    responsive : {
      // breakpoint from 768 up
      768 : {
          touchDrag: false
      }
    }
  });


  $('.prod-blocks-carousel').owlCarousel({
    loop: true,
    nav: true,
    dots: false,
    items: 1,
    autoplay: false,
    autoplayTimeout: 5000,
    mouseDrag: true,
    touchDrag: false,
    navSpeed: 1000,
    smartSpeed: 1000,
    responsive:{
      0:{
          items: 1
      },
      500:{
          autoWidth: true,
          margin: 40
      },
      1240:{
          autoWidth: true,
          margin: 40
      }
    }
  });

  // add-open-class
  $('.close-btn-i').click(function(e){
      e.stopPropagation();
      e.preventDefault();
      $('body').removeClass('head-banner-active');
      var id = $(this).data('id');
      var date = new Date(new Date().getTime() + 24*60*60*1000);
      document.cookie = "action"+id+"=1; path=/; expires=" + date.toUTCString();
  });

  $('.head-banner a').click(function(e){
      var id = $(this).find('.close-btn-i').data('id');
      var date = new Date(new Date().getTime() + 24*60*60*1000);
      document.cookie = "action"+id+"=1; path=/; expires=" + date.toUTCString();
  });


  // Scrool soc icons fixed
  $(window).scroll(function() {
    if ($(this).scrollTop() > $('.full-screen-img').height()){  
      $('.icons-aside').addClass("icons-sticky");
    }
    else{
      $('.icons-aside').removeClass("icons-sticky");
    }
  });


  // animation activate on desk screens (>= 1024px)
  var browserMinWidth = parseInt($('body').css('width'), 10);
  if (browserMinWidth > 1023) {
    $(window).enllax();
  }

    // if (browserMinWidth > 767) {
    //     $('.main-full-screen').each(function(){
    //         $(this).css('height', $(window).height()+'px');
    //     });
    // }

    if ($(window).width() >= 980) {
        // makes the parallax elements
        function parallaxIt() {
            // create variables
            var $fwindow = $(window);
            var scrollTop = window.pageYOffset || document.documentElement.scrollTop;

            var $contents = [];
            var $backgrounds = [];

            // for each of content parallax element
            $('[data-type="content"]').each(function (index, e) {
                var $contentObj = $(this);

                $contentObj.__speed = ($contentObj.data('speed') || 1);
                $contentObj.__fgOffset = $contentObj.offset().top;
                $contents.push($contentObj);
            });

            // for each of background parallax element
            $('[data-type="background"]').each(function () {
                var $backgroundObj = $(this);

                $backgroundObj.__speed = ($backgroundObj.data('speed') || 1);
                $backgroundObj.__fgOffset = $backgroundObj.offset().top;
                $backgrounds.push($backgroundObj);
            });

            // update positions
            $fwindow.on('scroll resize', function () {
                scrollTop = window.pageYOffset || document.documentElement.scrollTop;

                $contents.forEach(function ($contentObj) {
                    var yPos = $contentObj.__fgOffset - scrollTop / $contentObj.__speed;

                    $contentObj.css('top', yPos);
                })

                $backgrounds.forEach(function ($backgroundObj) {
                    var yPos = -((scrollTop - $backgroundObj.__fgOffset) / $backgroundObj.__speed);

                    $backgroundObj.css({
                        backgroundPosition: '50% ' + yPos + 'px'
                    });
                });
            });

            // triggers winodw scroll for refresh
            $fwindow.trigger('scroll');
        };

        parallaxIt();
    }
  
    $('.portf-carousel').each(function(){
        var owl = $('.portf-carousel');
        // var id = parseInt(window.location.hash.substr(1));
        // var n = id ? parseInt($('.portf-car-item[data-id='+id+']').data('n')) : 0;
        owl.owlCarousel({
            loop: true,
            nav: true,
            dots: false,
            items: 1,
            autoplay: false,
            autoplayTimeout: 5000,
            mouseDrag: false,
            touchDrag: false,
            autoHeight: true,
            lazyLoad: true,
            navSpeed: 1000,
            onInitialized: function(e){
                // var id = parseInt(window.location.hash.substr(1));
                // if (!id) { return; }
                // var n = parseInt($('.portf-car-item[data-id='+id+']').data('n'));
                // setTimeout(function(){
                //     owl.trigger('to.owl.carousel', n, 0);
                // }, 300);
            }
        });
    });
});



$(window).load(function() {

  $('.st-carousel').owlCarousel({
    loop: false,
    nav: true,
    dots: false,
    items: 1,
    autoHeight: true,
    mouseDrag: false,
    touchDrag: true,
    navSpeed: 1000,
    smartSpeed: 1000
  });

  $('.main-carousel').owlCarousel({
    loop: true,
    nav: true,
    dots: false,
    items: 1,
    autoplay: true,
    autoHeight: true,
    autoplayTimeout: 10000,
    mouseDrag: false,
    touchDrag: false,
    navSpeed: 1000,
    smartSpeed: 1000
  });

    $('.main-carousel').on('changed.owl.carousel', function(e) {
        setTimeout(function(){
            var data = $('.main-carousel .owl-item.active div').data();
            var c = $('.prod-full-slider-fix-pos .m-slider-info');
            c.find('.site-h1-tt').animate({
                opacity: 0
            }, 200, function() {
                var that = $(this);
                setTimeout(function(){
                    that.html(data.name).animate({ opacity: 1 }, 200);
                }, 250);
            });
            c.find('.site-h5-tt').animate({
                opacity: 0
            }, 200, function() {
                var that = $(this);
                setTimeout(function(){
                    that.html(data.annh).animate({ opacity: 1 }, 200);
                }, 250);
            });
            c.find('.ann-text').animate({
                opacity: 0
            }, 200, function() {
                var that = $(this);
                setTimeout(function(){
                    that.html(data.annt).animate({ opacity: 1 }, 200);
                }, 250);
            });
            c.find('.m-slider-year').animate({
                opacity: 0
            }, 200, function() {
                var that = $(this);
                setTimeout(function(){
                    that.html(data.year).animate({ opacity: 1 }, 200);
                }, 250);
            });
            // c.find('.site-h5-tt').fadeOut(500).html(data.annh).fadeIn(500);
            // c.find('.ann-text').fadeOut(500).html(data.annt).fadeIn(500);
            // c.find('.m-slider-year').fadeOut(500).html(data.year).fadeIn(500);
            c.find('a').attr('href', data.url);

            // c.find('.site-h1-tt').fadeOut(500).html(data.name).fadeIn(500);
            // c.find('.site-h5-tt').fadeOut(500).html(data.annh).fadeIn(500);
            // c.find('.ann-text').fadeOut(500).html(data.annt).fadeIn(500);
            // c.find('.m-slider-year').fadeOut(500).html(data.year).fadeIn(500);
            // c.find('a').attr('href', data.url);

            // c.find('.site-h1-tt').css('opacity', '0').html(data.name).css('opacity', '1');
            // c.find('.site-h5-tt').css('opacity', '0').html(data.annh).css('opacity', '1');
            // c.find('.ann-text').css('opacity', '0').html(data.annt).css('opacity', '1');
            // c.find('.m-slider-year').css('opacity', '0').html(data.year).css('opacity', '1');
            // c.find('a').attr('href', data.url);
        },100);
    });

  window.productOwlTranslate = false;
  window.productOwlDrag = false;
  var tmr, tmrr;
  $('.product-carousel').each(function(){
    var len = $(this).children().length;
    
    if (len > 1) {
      $(this).owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        items: 1,
        autoplay: false,
        autoHeight: true,
        autoplayTimeout: 10000,
        mouseDrag: false,
        touchDrag: true,
        navSpeed: 1000,
        smartSpeed: 1000,
        lazyLoad: true,
        lazyLoadEager: 2,
        responsive : {
          // breakpoint from 768 up
          768 : {
              touchDrag: false
          }
        }
      }).on('translate.owl.carousel', function(){
        clearTimeout(tmr);
        window.productOwlTranslate = true;
      }).on('translated.owl.carousel', function(){
        clearTimeout(tmr);
        tmr = setTimeout(function(){window.productOwlTranslate = false;}, 10);
      }).on('drag.owl.carousel', function(){
        clearTimeout(tmrr);
        window.productOwlDrag = true;
      }).on('dragged.owl.carousel', function(){
        clearTimeout(tmrr);
        tmrr = setTimeout(function(){window.productOwlDrag = false;}, 10);
      })
    }
  });

    $('.portf-carousel .owl-next').on('click', function (e) {
        var url = $('.owl-item.active div').data('url');
        history.pushState(null, null, url);
    });

    $('.portf-carousel .owl-prev').on('click', function (e) {
        var url = $('.owl-item.active div').data('url');
        history.pushState(null, null, url);
    });

  // animaton scroll effects
  $('.show-animation').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated',
    offset: 100
  });


  // add-open-class
    setTimeout(function(){
        $('body.banner-body').addClass('head-banner-active');
    }, 30000);

  $('.contacts-map').each(function(i,div){
        $.getScript('//maps.googleapis.com/maps/api/js?key=AIzaSyDL6p5injUWmdrEg2AcnDuWaZEJdodw0r4', function(){
            var uluru = {lat: $(div).data('x'), lng: $(div).data('y')};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: uluru,
                disableDefaultUI: true,
                // gestureHandling: 'none',
                styles: [
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#ffffff"}, {"lightness": 17}]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [{"color": "#ffffff"}, {"lightness": 18}]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [{"color": "#ffffff"}, {"lightness": 16}]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [{"color": "#dedede"}, {"lightness": 21}]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [{"visibility": "off"}]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#fefefe"}, {"lightness": 20}]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]
                    }
                ]
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map,
                icon: {
                    url: "/img/map-marker.svg",
                    scaledSize: new google.maps.Size(64, 64)
                }
            });
        });
    });

  $('.m-slide-fix-point').each(function(i,a){
      var top = $(a).data('top');
      var bottom = $(a).data('bottom');
      var imgHeight = $(a).data('hi');
      var imgWidth = $(a).data('wi');
      var windowHeight = $('.owl-stage').height();
      var windowWidth = $(window).width();
      var imgCurrentHeight = imgHeight/imgWidth*windowWidth;
      console.log(imgHeight + '/' + imgWidth + '*' + windowWidth);
      if (imgCurrentHeight <= windowHeight) {
          return;
      }

      var h = 0;
      if (top) {
          console.log(imgCurrentHeight +'*'+ top+'/'+100);
          h = (( imgCurrentHeight * top/100 - (imgCurrentHeight - windowHeight)/2 ) / windowHeight * 100);
          $(a).css('top', h+'%');
      }

      if (bottom) {
          h = (( imgHeight * bottom/100 - (imgHeight - windowHeight)/2 ) / windowHeight * 100);
          $(a).css('bottom', h+'%');
      }
  });


  $(".scrollbar-catalog").mCustomScrollbar({
    mouseWheel: { 
      scrollAmount: 150 
    }
  });

  var lazyLoadInstance = new LazyLoad({
      elements_selector: ".lazy"
  });

  // auto height offset for mobile filter
  if (document.querySelector(".aside-catalog-inner")) {
    document.querySelector(".aside-catalog-inner").style.borderTopWidth = document.querySelector(".catalog-head").offsetHeight + 'px';
  }


});



/* animation scroll */
!function(a){a.fn.viewportChecker=function(b){var c={classToAdd:"visible",classToRemove:"invisible",classToAddForFullView:"full-visible",removeClassAfterAnimation:!1,offset:100,repeat:!1,invertBottomOffset:!0,callbackFunction:function(a,b){},scrollHorizontal:!1,scrollBox:window};a.extend(c,b);var d=this,e={height:a(c.scrollBox).height(),width:a(c.scrollBox).width()};return this.checkElements=function(){var b,f;c.scrollHorizontal?(b=Math.max(a("html").scrollLeft(),a("body").scrollLeft(),a(window).scrollLeft()),f=b+e.width):(b=Math.max(a("html").scrollTop(),a("body").scrollTop(),a(window).scrollTop()),f=b+e.height),d.each(function(){var d=a(this),g={},h={};if(d.data("vp-add-class")&&(h.classToAdd=d.data("vp-add-class")),d.data("vp-remove-class")&&(h.classToRemove=d.data("vp-remove-class")),d.data("vp-add-class-full-view")&&(h.classToAddForFullView=d.data("vp-add-class-full-view")),d.data("vp-keep-add-class")&&(h.removeClassAfterAnimation=d.data("vp-remove-after-animation")),d.data("vp-offset")&&(h.offset=d.data("vp-offset")),d.data("vp-repeat")&&(h.repeat=d.data("vp-repeat")),d.data("vp-scrollHorizontal")&&(h.scrollHorizontal=d.data("vp-scrollHorizontal")),d.data("vp-invertBottomOffset")&&(h.scrollHorizontal=d.data("vp-invertBottomOffset")),a.extend(g,c),a.extend(g,h),!d.data("vp-animated")||g.repeat){String(g.offset).indexOf("%")>0&&(g.offset=parseInt(g.offset)/100*e.height);var i=g.scrollHorizontal?d.offset().left:d.offset().top,j=g.scrollHorizontal?i+d.width():i+d.height(),k=Math.round(i)+g.offset,l=g.scrollHorizontal?k+d.width():k+d.height();g.invertBottomOffset&&(l-=2*g.offset),k<f&&l>b?(d.removeClass(g.classToRemove),d.addClass(g.classToAdd),g.callbackFunction(d,"add"),j<=f&&i>=b?d.addClass(g.classToAddForFullView):d.removeClass(g.classToAddForFullView),d.data("vp-animated",!0),g.removeClassAfterAnimation&&d.one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",function(){d.removeClass(g.classToAdd)})):d.hasClass(g.classToAdd)&&g.repeat&&(d.removeClass(g.classToAdd+" "+g.classToAddForFullView),g.callbackFunction(d,"remove"),d.data("vp-animated",!1))}})},("ontouchstart"in window||"onmsgesturechange"in window)&&a(document).bind("touchmove MSPointerMove pointermove",this.checkElements),a(c.scrollBox).bind("load scroll",this.checkElements),a(window).resize(function(b){e={height:a(c.scrollBox).height(),width:a(c.scrollBox).width()},d.checkElements()}),this.checkElements(),this}}(jQuery);




//Plugin placeholder
(function(b,f,i){function l(){b(this).find(c).each(j)}function m(a){for(var a=a.attributes,b={},c=/^jQuery\d+/,e=0;e<a.length;e++)if(a[e].specified&&!c.test(a[e].name))b[a[e].name]=a[e].value;return b}function j(){var a=b(this),d;a.is(":password")||(a.data("password")?(d=a.next().show().focus(),b("label[for="+a.attr("id")+"]").attr("for",d.attr("id")),a.remove()):a.realVal()==a.attr("placeholder")&&(a.val(""),a.removeClass("placeholder")))}function k(){var a=b(this),d,c;placeholder=a.attr("placeholder");
b.trim(a.val()).length>0||(a.is(":password")?(c=a.attr("id")+"-clone",d=b("<input/>").attr(b.extend(m(this),{type:"text",value:placeholder,"data-password":1,id:c})).addClass("placeholder"),a.before(d).hide(),b("label[for="+a.attr("id")+"]").attr("for",c)):(a.val(placeholder),a.addClass("placeholder")))}var g="placeholder"in f.createElement("input"),h="placeholder"in f.createElement("textarea"),c=":input[placeholder]";b.placeholder={input:g,textarea:h};!i&&g&&h?b.fn.placeholder=function(){}:(!i&&g&&
!h&&(c="textarea[placeholder]"),b.fn.realVal=b.fn.val,b.fn.val=function(){var a=b(this),d;if(arguments.length>0)return a.realVal.apply(this,arguments);d=a.realVal();a=a.attr("placeholder");return d==a?"":d},b.fn.placeholder=function(){this.filter(c).each(k);return this},b(function(a){var b=a(f);b.on("submit","form",l);b.on("focus",c,j);b.on("blur",c,k);a(c).placeholder()}))})(jQuery,document,window.debug);


/* mask input*/
(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);


//checkbox
!function(e){e.addModule(function(e){"use strict";return{name:"Checkbox",selector:'input[type="checkbox"]',options:{wrapNative:!0,checkedClass:"jcf-checked",uncheckedClass:"jcf-unchecked",labelActiveClass:"jcf-label-active",fakeStructure:'<span class="jcf-checkbox"><span></span></span>'},matchElement:function(e){return e.is(":checkbox")},init:function(){this.initStructure(),this.attachEvents(),this.refresh()},initStructure:function(){this.doc=e(document),this.realElement=e(this.options.element),this.fakeElement=e(this.options.fakeStructure).insertAfter(this.realElement),this.labelElement=this.getLabelFor(),this.options.wrapNative?this.realElement.appendTo(this.fakeElement).css({position:"absolute",height:"100%",width:"100%",opacity:0,margin:0}):this.realElement.addClass(this.options.hiddenClass)},attachEvents:function(){this.realElement.on({focus:this.onFocus,click:this.onRealClick}),this.fakeElement.on("click",this.onFakeClick),this.fakeElement.on("jcf-pointerdown",this.onPress)},onRealClick:function(e){var t=this;this.savedEventObject=e,setTimeout(function(){t.refresh()},0)},onFakeClick:function(e){this.options.wrapNative&&this.realElement.is(e.target)||this.realElement.is(":disabled")||(delete this.savedEventObject,this.stateChecked=this.realElement.prop("checked"),this.realElement.prop("checked",!this.stateChecked),this.fireNativeEvent(this.realElement,"click"),this.savedEventObject&&this.savedEventObject.isDefaultPrevented()?this.realElement.prop("checked",this.stateChecked):this.fireNativeEvent(this.realElement,"change"),delete this.savedEventObject)},onFocus:function(){this.pressedFlag&&this.focusedFlag||(this.focusedFlag=!0,this.fakeElement.addClass(this.options.focusClass),this.realElement.on("blur",this.onBlur))},onBlur:function(){this.pressedFlag||(this.focusedFlag=!1,this.fakeElement.removeClass(this.options.focusClass),this.realElement.off("blur",this.onBlur))},onPress:function(e){this.focusedFlag||"mouse"!==e.pointerType||this.realElement.focus(),this.pressedFlag=!0,this.fakeElement.addClass(this.options.pressedClass),this.doc.on("jcf-pointerup",this.onRelease)},onRelease:function(e){this.focusedFlag&&"mouse"===e.pointerType&&this.realElement.focus(),this.pressedFlag=!1,this.fakeElement.removeClass(this.options.pressedClass),this.doc.off("jcf-pointerup",this.onRelease)},getLabelFor:function(){var t=this.realElement.closest("label"),s=this.realElement.prop("id");return!t.length&&s&&(t=e('label[for="'+s+'"]')),t.length?t:null},refresh:function(){var e=this.realElement.is(":checked"),t=this.realElement.is(":disabled");this.fakeElement.toggleClass(this.options.checkedClass,e).toggleClass(this.options.uncheckedClass,!e).toggleClass(this.options.disabledClass,t),this.labelElement&&this.labelElement.toggleClass(this.options.labelActiveClass,e)},destroy:function(){this.options.wrapNative?this.realElement.insertBefore(this.fakeElement).css({position:"",width:"",height:"",opacity:"",margin:""}):this.realElement.removeClass(this.options.hiddenClass),this.fakeElement.off("jcf-pointerdown",this.onPress),this.fakeElement.remove(),this.doc.off("jcf-pointerup",this.onRelease),this.realElement.off({focus:this.onFocus,click:this.onRealClick})}}})}(jcf);

//radio
!function(e){e.addModule(function(t){"use strict";return{name:"Radio",selector:'input[type="radio"]',options:{wrapNative:!0,checkedClass:"jcf-checked",uncheckedClass:"jcf-unchecked",labelActiveClass:"jcf-label-active",fakeStructure:'<span class="jcf-radio"><span></span></span>'},matchElement:function(e){return e.is(":radio")},init:function(){this.initStructure(),this.attachEvents(),this.refresh()},initStructure:function(){this.doc=t(document),this.realElement=t(this.options.element),this.fakeElement=t(this.options.fakeStructure).insertAfter(this.realElement),this.labelElement=this.getLabelFor(),this.options.wrapNative?this.realElement.prependTo(this.fakeElement).css({position:"absolute",opacity:0}):this.realElement.addClass(this.options.hiddenClass)},attachEvents:function(){this.realElement.on({focus:this.onFocus,click:this.onRealClick}),this.fakeElement.on("click",this.onFakeClick),this.fakeElement.on("jcf-pointerdown",this.onPress)},onRealClick:function(e){var t=this;this.savedEventObject=e,setTimeout(function(){t.refreshRadioGroup()},0)},onFakeClick:function(e){this.options.wrapNative&&this.realElement.is(e.target)||this.realElement.is(":disabled")||(delete this.savedEventObject,this.currentActiveRadio=this.getCurrentActiveRadio(),this.stateChecked=this.realElement.prop("checked"),this.realElement.prop("checked",!0),this.fireNativeEvent(this.realElement,"click"),this.savedEventObject&&this.savedEventObject.isDefaultPrevented()?(this.realElement.prop("checked",this.stateChecked),this.currentActiveRadio.prop("checked",!0)):this.fireNativeEvent(this.realElement,"change"),delete this.savedEventObject)},onFocus:function(){this.pressedFlag&&this.focusedFlag||(this.focusedFlag=!0,this.fakeElement.addClass(this.options.focusClass),this.realElement.on("blur",this.onBlur))},onBlur:function(){this.pressedFlag||(this.focusedFlag=!1,this.fakeElement.removeClass(this.options.focusClass),this.realElement.off("blur",this.onBlur))},onPress:function(e){this.focusedFlag||"mouse"!==e.pointerType||this.realElement.focus(),this.pressedFlag=!0,this.fakeElement.addClass(this.options.pressedClass),this.doc.on("jcf-pointerup",this.onRelease)},onRelease:function(e){this.focusedFlag&&"mouse"===e.pointerType&&this.realElement.focus(),this.pressedFlag=!1,this.fakeElement.removeClass(this.options.pressedClass),this.doc.off("jcf-pointerup",this.onRelease)},getCurrentActiveRadio:function(){return this.getRadioGroup(this.realElement).filter(":checked")},getRadioGroup:function(e){var s=e.attr("name"),i=e.parents("form");return s?i.length?i.find('input[name="'+s+'"]'):t('input[name="'+s+'"]:not(form input)'):e},getLabelFor:function(){var e=this.realElement.closest("label"),s=this.realElement.prop("id");return!e.length&&s&&(e=t('label[for="'+s+'"]')),e.length?e:null},refreshRadioGroup:function(){this.getRadioGroup(this.realElement).each(function(){e.refresh(this)})},refresh:function(){var e=this.realElement.is(":checked"),t=this.realElement.is(":disabled");this.fakeElement.toggleClass(this.options.checkedClass,e).toggleClass(this.options.uncheckedClass,!e).toggleClass(this.options.disabledClass,t),this.labelElement&&this.labelElement.toggleClass(this.options.labelActiveClass,e)},destroy:function(){this.options.wrapNative?this.realElement.insertBefore(this.fakeElement).css({position:"",width:"",height:"",opacity:"",margin:""}):this.realElement.removeClass(this.options.hiddenClass),this.fakeElement.off("jcf-pointerdown",this.onPress),this.fakeElement.remove(),this.doc.off("jcf-pointerup",this.onRelease),this.realElement.off({blur:this.onBlur,focus:this.onFocus,click:this.onRealClick})}}})}(jcf);